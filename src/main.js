/*=========================================================================================
  File Name: main.js
  Description: main vue(js) file
  ----------------------------------------------------------------------------------------
  Item Name: Vuesax Admin - VueJS Dashboard Admin Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


import Vue from 'vue'
import App from './App.vue'

// Vuesax Component Framework
import Vuesax from 'vuesax'
import 'material-icons/iconfont/material-icons.css' //Material Icons
import 'vuesax/dist/vuesax.css'; // Vuesax
Vue.use(Vuesax)


// Theme Configurations
import '../themeConfig.js'


// Globally Registered Components
import './globalComponents.js'


// Styles: SCSS
import './assets/scss/main.scss'


// Tailwind
import '@/assets/css/main.css';


// Vue Router
import router from './router'


// Vuex Store
import store from './store/store'


// Vuesax Admin Filters
import './filters/filters'

// Vuejs - Vue wrapper for hammerjs
import { VueHammer } from 'vue2-hammer'
Vue.use(VueHammer)


// PrismJS
import 'prismjs'
import 'prismjs/themes/prism-tomorrow.css'


// Feather font icon
require('./assets/css/iconfont.css')

import axios from 'axios';

Vue.prototype.$http = axios;
//server
Vue.prototype.$http.defaults.baseURL = 'http://localhost:8000/'; 

Vue.config.productionTip = false





router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    if (!store.state.user) {
      next({
        path: 'pages/login/',
        // query: { redirect: to.fullPath }
      })
    } else {
      next()
    }
  } else {
    next() // make sure to always call next()!
  }
})


// router.afterEach((to) => {
//   const menuName = to.meta.menuName

//   if(store.state.user) {
//     let access = null
//     store.state.user.groupaccess.map(item => {
//       if(menuName.toUpperCase() == item.menu.menu.toUpperCase()) {
//         access = item
//       }
//     })

//     store.commit('SET_ACCESS_STATE', access)
//   }
// })

let vm = new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')

global.vm = vm

let timeout = 180000
let timeoutId

function startTimer() {
  timeoutId = window.setTimeout(doInactive, timeout)
}

function doInactive() {
  if(store.state.user) {
    location.reload()
  }
}

function resetTimer() {
  window.clearTimeout(timeoutId)
  startTimer()
}

function setupTimers () {
  document.addEventListener("mousemove", resetTimer, false)
  document.addEventListener("mousedown", resetTimer, false)
  document.addEventListener("keypress", resetTimer, false)
  document.addEventListener("touchmove", resetTimer, false)
   
  startTimer()
}

setupTimers()
// setInterval(function() {
//   if(store.state.user) {
//     Vue.prototype.$http.post('/login')
//   }
// }, 60000)